/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.javeriana.services;

import co.edu.javeriana.model.Cuenta;
import co.edu.javeriana.model.Respuesta;
import co.edu.javeriana.utils.EstatusCode;
import java.math.BigInteger;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;

/**
 *
 * @author carlos
 */
@WebService(serviceName = "cuenta")
public class CuentaService {

    /**
     * Metodo que consulta el saldo de una cuenta
     * @param cuenta {@link Cuenta}
     * @return {@link Cuenta}
     */
    @WebMethod(operationName = "consultarSaldo")
    public Cuenta consultarSaldo(@WebParam(name = "cuenta") Cuenta cuenta) {
        cuenta.setSaldoCuenta(200404);
        return cuenta;
    }
    
    @WebMethod(operationName = "descontarValor")
    public Respuesta descontarValor(@WebParam(name = "cuentaDescuento") Cuenta cuenta) {
        
        Respuesta respuesta = new Respuesta();
        if(cuenta.getNumeroCuenta() == BigInteger.valueOf(555555) || cuenta.getNumeroCuenta() == null ){
            respuesta.setCodigoRespuesta(BigInteger.valueOf(EstatusCode.HTTP_404));
            respuesta.setMessage("Cuenta no encontrada");
        } else {
            respuesta.setCodigoRespuesta(BigInteger.valueOf(EstatusCode.HTTP_200));
            respuesta.setMessage("Valor descontado exitosamente" + cuenta.getValorDescuento());
        }
        return respuesta;
    }
}
