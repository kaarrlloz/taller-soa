package co.edu.javeriana.services;

import java.math.BigInteger;

import javax.jws.WebMethod;
import javax.jws.WebService;

import co.edu.javeriana.model.Convenio;
import co.edu.javeriana.model.Factura;

@WebService
public class TransformarServicio {

	@WebMethod
	public String obtenerEnvelope(Convenio convenio, Factura factura) {

		if (factura != null && convenio != null) {
			if (convenio.getIdConvenio().intValue() == 11000) {
				String envelope = "/servicios/pagos/v1/payments/"+factura.getNumeroFactura();
				return envelope;
			}
			if (convenio.getIdConvenio().intValue() == 11001) {
				String envelope = 
						"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:sch=\"http://www.servicios.co/pagos/schemas\">"
						+ "<soapenv:Header/>"
						+ "<soapenv:Body>"
						+ "<sch:Pago>"
						+ "<sch:referenciaFactura>"
						+ "<sch:referenciaFactura>"+factura.getNumeroFactura()+"</sch:referenciaFactura>"
						+ "<sch:referenciaFactura>"
						+ "<sch:totalPagar>"+factura.getValorFactura()+"</sch:totalPagar>"
						+ "</sch:Pago>"
						+ "</soapenv:Body>"
						+ "</soapenv:Envelope>";
				return envelope;
			}
		}

		return null;
	}
}
