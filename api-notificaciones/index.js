'use strict'

const express = require('express')
const bodyParser = require('body-parser')
const config = require('./config')


const app = express()
const api = require('./routes')

app.use(bodyParser.urlencoded({extended: true}))
app.use(bodyParser.json())

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, authorization");
  res.header("Access-Control-Allow-Methods", "POST, GET, PUT, DELETE, OPTIONS");
  next();
});

app.use('/api', api)

app.listen(config.port, () =>{
            console.log(`API Rest SIS Planer http://localhost:${config.port}!`)
        })

module.exports = app