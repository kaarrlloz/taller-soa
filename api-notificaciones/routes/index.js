'use strict'

const express = require('express')
const api = express.Router()

const servicio = require('../services')

api.post('/notificacion', servicio.enviarNotificacion)

module.exports = api