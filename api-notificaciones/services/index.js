'use strict'

const config = require('../config')
const express = require('express')
const mailer = require('express-mailer');

const mailapp = express()
mailapp.set('views', 'services');
mailapp.set('view engine', 'jade');

mailer.extend(mailapp, {
  from: 'sisplanner.notify@gmail.com',
  host: 'smtp.gmail.com',
  secureConnection: true,
  port: 465,
  transportMethod: 'SMTP',
  auth: {
    user: 'sisplanner.notify@gmail.com',
    pass: '7l8o45gv'
  }
});



function enviarNotificacion(req, res) {

  var mailOptions = req.body;
  console.log("entre al servicio");
  console.log(mailOptions);

  if (mailOptions == undefined || mailOptions == ""
      || mailOptions.template == undefined) {
    res.status(500).send({codigoRespuesta:500,message:"Error de datos de entrada"})
    return
  }
  
  mailapp.mailer.send(mailOptions.template, mailOptions, function (err, message) {
    if (err) {
      res.status(500).send({codigoRespuesta:500,message:"Error enviando la notificación"})
      return;
    }
    console.log('Mensaje Enviado');
     res.status(200).send({codigoRespuesta:200,message:"Notificacion enviada Exitosamente"})
     return
  });

}

module.exports = { enviarNotificacion }
