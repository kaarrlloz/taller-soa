/**
 * 
 */
package co.edu.javeriana.WSVerificadorContenido;

import java.math.BigInteger;

import javax.jws.WebMethod;
import javax.jws.WebService;


import co.edu.javeriana.WSVerificadorContenido.LoadProperties;
import co.edu.javeriana.model.Convenio;
import co.edu.javeriana.model.Factura;


/**
 * @author linda
 *
 */
@WebService(serviceName = "wsVerificadorContenido")
public class wsVerificadorContenido {

	/**
	 * 
	 */
	private LoadProperties properties;
	private String[] convenios;
	public wsVerificadorContenido() {
		// TODO Auto-generated constructor stub
		
	}
	@WebMethod
	public Convenio verificarContenido(Factura fact){
		String urlConvenio = new String();
		motorReglas();
		Convenio convenioRedirect = new Convenio();
		
		for (int i =0; i<convenios.length; i=i+3) {
			//Integer nf = fact.getNumeroFactura();
			String numFact = fact.getNumeroFactura() ;
			System.out.println("numero factura que llega:  "+numFact);
			if(numFact.contains(convenios[i])){				
				urlConvenio =  convenios[i+1];
				System.out.println("end point a retornar "+convenios[i+1]);
				convenioRedirect.setEndPoint(convenios[i+1]);				
				convenioRedirect.setNombreConvenio(convenios[i+2]);				
				convenioRedirect.setIdConvenio(BigInteger.valueOf(Integer.parseInt(convenios[i])));				
			}			
		}
		System.out.println("url del convenio retornado: "+convenioRedirect.getEndPoint());
		return convenioRedirect;				
	}
	public void motorReglas(){
		this.properties = LoadProperties.getInstance();
		this.convenios = properties.getConvenios();
	}
}
