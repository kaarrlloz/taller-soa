/**
 * 
 */
package co.edu.javeriana.WSVerificadorContenido;

import java.util.ArrayList;
import java.util.List;

/**
 * @author linda
 *
 */

import java.util.Properties;



public  class  LoadProperties {
	
	private static LoadProperties instance = null;
	
	private String energia;
	private String agua;
	private String[] convenios;
	
	
    
    public static LoadProperties getInstance() {
        if(instance == null) {
           instance = new LoadProperties();
        }
        return instance;
     }
	
	protected LoadProperties() {
		try {
			Properties properties = new Properties();
			properties.load(getClass().getResourceAsStream("/co/edu/javeriana/WSVerificadorContenido/reglas.properties"));
			
			energia = properties.getProperty("energia");
			agua = properties.getProperty("agua");
			convenios =  properties.getProperty("convenios").split(",");
			
			

		}catch(Exception e) {
			System.out.println("-------> ERROR: LOADING THE FILE PROPERTIES <-------");
		}
	}	
	

	public String getEnergia() {
		return energia;
	}

	public String getAgua() {
		return agua;
	}

	public String[] getConvenios() {
		return convenios;
	}
	
}
