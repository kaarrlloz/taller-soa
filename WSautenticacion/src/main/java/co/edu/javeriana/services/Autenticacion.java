package co.edu.javeriana.services;

import javax.jws.WebMethod;
import javax.jws.WebService;

import co.edu.javeriana.model.Usuario;

@WebService
public class Autenticacion {

	@WebMethod
	public Usuario autenticarUsuario(Usuario usuario){
		
		if(usuario!=null){
			if(usuario.getEmail()!=null && !usuario.getEmail().isEmpty()){
				if(usuario.getContrasena() !=null && !usuario.getContrasena().isEmpty()){
					usuario.setIdUsuario(1000);
					usuario.setNumeroDocumento("1014208598");
					usuario.setTipoDocumento("CC");
					return usuario;
				}
			}
		}
		return usuario;
	}
}
