TALLER SERVICIOS SOA (BANCO ABC)

INTEGRANTES:
Linda Polanía
Francisco Argüello
Carlos Marín
Alejandro Rodríguez
Antony Guarnizo

OBJETIVO
Crear un conjunto de servicios soporten las alianzas realizadas por parte del banco ABC con Entidades de pago,  que permitan la 
interoperabilidad con las entidades que prestan servicios públicos, teniendo en cuenta que estas, nos proveen servicios web con 
diferentes protocolos, en donde los servicios deberán estar en la capacidad de automatizar completamente las solicitudes de los 
usuarios del banco.

JUSTIFICACIONES DE ARQUITECTURA

Teniendo en cuenta los requerimientos del banco ABC, relacionados con poder implementar convenios con los proveedores de servicios
públicos para el pago de los mismos a través de sus canales de servicios, se toman las siguientes decisiones arquitecturales:

* El estilo de arquitectura definido es “Publisher/suscriber” el cual nos permitirá el registro de  nuevos proveedores 
de convenios con los cuales el banco pueda llegar a realizar negociación sin necesidad de modificar en gran medida la arquitectura
o los componentes de la misma.

* Se implementará el patrón “inmmediate routing” el cual se encargará de distribuir las comunicaciones hacia los servicios 
seleccionados por el usuario.

* Se utilizará el patrón “service broker” para realizar transformaciones entre los servicios a través de archivos xslt y así permitir 
las diferentes conexiones entre los proveedores de convenios sin importar el protocolo que utilicen

* El patrón “enterprise inventory” se usará para clasificar y exponer los servicios de envío de notificaciones y autenticación, 
los cuales podrán ser reutilizados por otras entidades de negocio dentro del banco.

* Se implementará el patrón “domain inventory” para la clasificación de los servicios propios del dominio de negocio que se está arquitecturando.

* Se utilizará el patrón “atomic services transaction” para manejar servicios de compensación en las transacciones realizadas, es decir, brindar seguridad y 
confianza en la transaccionalidad que se dará hacia los servicios de las entidades públicas.

* Se implementará el patrón “Legacy wraper” para encapsular los servicios de la aplicación de consulta de saldos, el cual es un sistema 
legado que no se piensa modificar en este momento.

* Cabe anotar que los servicios se diseñarán teniendo en cuenta los ocho principios enmarcados dentro de una estrategia SOA. 


MODELO CANÓNICO
Con el objetivo de tener una estandarización de los servicios a nivel interno del banco, se crea un modelo canónico el cual expone las entidades 
de negocio de una manera  entendible y que así mismo puedan ser consumidos por diferentes líneas de negocio dentro del banco. 
Las entidades identificadas y definidas son:

* Pago
* Factura
* Convenio
* Usuario
* Cuenta
* Notificación

SERVICIOS QUE COMPONEN LA SOLUCIÓN

Los servicios que se construirán son los siguientes:

* VerificarContenido: Este servicio se encarga de validar el número de la factura enviada en la solicitud del usuario de pagar servicio público,
para así conocer a qué convenio se dirige la comunicación.

* ServicioAutenticacion: Con este servicio se habilita la autenticación hacía la aplicación SAP

* ServicioNotificaciones: Encargado del envio de notificaciones que puedan ser causadas en cualquier parte dell proceso.

* TransformacionServices: El servicio se encarga de realizar las transformaciones necesarias para el intercambio de información entre 
los diferentes convenios que tiene la entidad, esto basado en que cada uno de ellos puede manejar diferentes protocolos.

* VerificacionSaldosDescuentosCuentas: Verifica el estado de la cuenta en términos de saldos y descuentos.

* Dispatcher: Encargado de enrutar las peticiones a las entidades.
